aws_ec2_instance
=========

Provision a VM in AWS EC2  

You may want to use ansible-galaxy and a requirements file to download this
role for use. An example `requirements.yml` file is shown.
```
---
- src: "git+https://gitlab.com/paultsparks-ansible-roles/aws_ec2_instance.git"
  name: ec2_instance
```
If you want to keep downloaded roles separate from your own roles. You may wish
to set `roles_path = ext_roles` in your `ansible.cfg` file and add `ext-roles/`
to your `.gitignore` file.

Requirements
------------

Fairly recent version of boto (> 2.49.0 ?)  
Fairly recent version of boto3 (> 1.8 ?)  
For include_role, the IAM *PassRole* permission is required. The IAM permissions
*ListInstanceProfiles* and *GetInstanceProfile* are required to add by name. It is
highly recommended to limit the Resources on the *PassRole* permission to specific
roles to prevent assignment of overly broad permissions. More info
[here](https://aws.amazon.com/blogs/security/granting-permission-to-launch-ec2-instances-with-iam-roles-passrole-permission/).  
Probably others I'm missing right now.

Role Variables
--------------
Takes a single `values` dictionary variable with the following parameters for 
creating a VM:

**image_id** or **image_name** or **image** - *Required for creation.* AMI ID or AMI Name
or image object (see https://docs.ansible.com/ansible/latest/modules/ec2_instance_module.html)

**ssh_key_name** - *Required for creation.* Name of AWS SSH key to inject into
the image for SSH access.  

**vpc_name** or **vpc_id** - *Required if **sg_rules** or **sg_egress** 
is supplied* AWS target VPC. If both are given, **vpc_id** takes precedence.

**ami_volumes** - Access to the Ansible ec2_instance *volumes* argument. Use for setting AMI volume options.
Use **volumes** for adding additional volumes.

**assign_public_ip** - If `yes` a public IP address will be assigned to the VM.
Default is `no`.  

**availability_zone** - Zone to use if **vpc_subnet_id** is not specified. If no subnet,
ENI, or availability zone is provided, the default subnet in the default VPC will be
used in the first AZ (alphabetically sorted)

**cpu_credit_specification** - When set to `unlimited` allow increased charges 
if CPU credits are depleted. Default is `standard`.

**cpu_options** - Reduce vCPUs for instance.
(see https://docs.ansible.com/ansible/latest/modules/ec2_instance_module.html)

**detailed_monitoring** - If `yes`, allow detailed cloudwatch metrics to be collected. Defaults to`no`.

**ebs_optimized** - If `yes`, use EBS optimized volumes. Defaults to `no`.

**instance_initiated_shutdown_behavior** - `stop` or `terminate`

**instance_role** - ARN or name of an EC2 instance role to apply to the VM. 
Special IAM permissions are required; see Requirements section above.

**instance_type** - AWS instance type ("T-shirt" size. e.g. t2.small). Defaults 
to `t2.micro`.  

**launch_template** - A launch template to base instance on. See https://docs.ansible.com/ansible/latest/modules/ec2_instance_module.html for details.

**placement_group** - Placement group to assign to the instance.

**preserve_instance_volume** - *For `state: absent` only.* If set to `yes`, the root
volume for the instance will not be deleted (for EBS volumes). Defaults to `no`.

**region** - AWS region name (e.g. "us-east-1")  If not specified, uses the
AWS_REGION or EC2_REGION environment variable value.  

**rename_prefix** - *For `state: absent` only.* The prefix used to rename instances
before termination. Terminated EC2 instances remain visible for up to an hour. Later
(re)creation of instances could have duplicate names and cause issues.
Default prefix is `ZZ_`; use `''` to prevent renaming.

**sg_names** - List of names of already defined AWS security groups in the target VPC.
 If no security group information is supplied, the VPC `default`
security group will be used.

**sg_rules** and / or **sg_egress** -  List of inbound and / or outbound rules 
to create a security group. See [Ansible ec2_group module](https://docs.ansible.com/ansible/latest/modules/ec2_group_module.html) rules field. 
If **sg_names** is also specified, the newly created group will be appended
to the sg_names list. If no security group information is supplied, the VPC `default`
security group will be used.  

**sg_tags** - Optional hash/dictionary of tags for the Security Group if you 
specify **sg_rules** and / or **sg_egress**.  

**state** - Desired state of the VM. One of `present`, `started`, `running`,
`absent`, `stopped`, `start`. The value `start` should be used to start an existing
instance. Default is `running`.  

**subnet_name** or **subnet_id** - The subnet to place the VM. If not specified,
the default zone of the default VPC will be used.

**tags** - Optional hash / dictionary of tags for your VM  

**tenancy** - If `dedicated`, run on dedicated hardware with additional cost. Default is `shared`.

**termination_protection** - If `yes`, termination protection must be disabled before terminating instance.
Default is `no`.

**tower_callback** - Preconfigured user-data to enable an instance to perform a Tower callback (Linux only).
Mutually exclusive with **user_data**. See https://docs.ansible.com/ansible/latest/modules/ec2_instance_module.html
for details and Windows instructions.

**user_data** - Cloud-init user data for VM creation. See
[AWS cloud-init docs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/amazon-linux-ami-basics.html#amazon-linux-cloud-init)
for formats.  

**vm_name** - Name for your VM. defaults to `myvm`  

**volumes** - Optional list of dictionaries of volumes to create and attach to
the instance. Requires **name**, **volume_size**. Optional values are **delete_on_termination**,
**device_name**, **encrypted**, **iops**, **kms_key_id**, **snapshot**, **tags**,
**volume_type**, and **zone**. See [Ansible ec2_vol](https://docs.ansible.com/ansible/latest/modules/ec2_vol_module.html#ec2-vol-module)
for details.

#### Authorization values

**aws_access_key** - AWS access key value. If not set then the value of the
`AWS_ACCESS_KEY_ID`, `AWS_ACCESS_KEY` or `EC2_ACCESS_KEY` environment variable is used.

**aws_secret_key** - AWS secret key value. If not set then the value of the
`AWS_SECRET_ACCESS_KEY`, `AWS_SECRET_KEY`, or `EC2_SECRET_KEY` environment variable is used.

**boto_profile** - A boto profile name.

**security_token** - AWS STS security token.  If not set then the value of the
`AWS_SECURITY_TOKEN` or `EC2_SECURITY_TOKEN` environment variable is used.

Removing VMs
-------------

For removing VMs the following parameters may be used: **vm_name**,
**region**, **preserve_instance_volume**, and **rename_prefix**.

Returned Information
--------------------

Returns instance information in variable **ec2_instance**.
See [Ansible ec2_instance](https://docs.ansible.com/ansible/latest/modules/ec2_instance_module.html#return-values)
for details


Dependencies
------------

None


Example Playbook
----------------
This playbook has a couple of tasks at the beginning to determine an AMI ID. You
may have an AMI ID defined elsewhere and probably do not need these tasks.
```
- hosts: localhost
  tasks:

  # Determining an AMI ID. You may want to skip this and the next task.
  - name: Find Ubuntu AMI
    ec2_ami_facts:
      filters:
        state: [ "available" ]
        owner-id: [ "513442679011" ]
        architecture: [ "x86_64" ]
        virtualization-type: [ "hvm" ]
        description: "Canonical, Ubuntu, 16.04 LTS,*"
        root-device-type: "ebs"
    register: tmp_amis

  - name: Find latest Ubuntu image
    set_fact:
      ec2_ami: "{{ tmp_amis.images |sort(reverse=True, attribute='name') |first }}"

  # HERE'S THE ACTUAL CALL 
  - name: Create EC2 instance
    include_role:
      name: ec2_instance
    vars:
      values:
        region: "{{ cfg.aws.region }}"
        vpc_name: "{{ cfg_aws.vpc_name }}"
        vm_name: "guacamole-server"
        image_id: "{{ec2_ami.image_id}}"
        instance_type: "t2.small"
        ssh_key_name: "{{ cfg_aws.ssh_key_name }}"
        sg_rules:
          - proto: tcp
            ports: [22]
            cidr_ip: "{{allow_ssh_cidr}}"
            rule_desc: "Allow SSH from restricted IP range"
          - proto: tcp
            ports: [443]
            cidr_ip: 0.0.0.0/0
            rule_desc: "Allow HTTPS from anywhere"
        volumes:
          - name: logs
            volume_size: 100
            volume_type: st1
            delete_on_termination: no

  # Example of how to use the information returned by the ec2_instance role.
  # You may not need these last tasks.
  - name: Save instance information for server
    set_fact: 
      instance_id: "{{ ec2_instance.instance_id }}"
      instance_dns_name: "{{ ec2_instance.public_dns_name }}"

  - name: Add VM to inventory
    add_host:
      name: "guacamole-server"
      groups: "aws"
      ansible_host: "{{ instance_dns_name }}"
```

License
-------

MIT

Author Information
------------------


